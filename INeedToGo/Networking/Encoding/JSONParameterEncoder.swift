//
//  JSONParameterEncoder.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import Foundation

/**
    Parameters are encoded to JSON and add appropriate headers
 */
struct JSONParameterEncoder: ParameterEncoder {
    
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            urlRequest.httpBody = jsonAsData
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
        
    }
    
}
