//
//  RestroomResponse.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/21/21.
//

import Foundation

/**
    This response is used to store the data response from the Restroom Search API
 */
struct RestroomResponse {
    let restrooms: [Restroom]
}

extension RestroomResponse: Decodable {
    
    enum RestroomResponseCodingKeys: String, CodingKey {
        case restrooms
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        restrooms = try container.decode([Restroom].self)
    }
}
