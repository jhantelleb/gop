//
//  RestRoomAPI.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import UIKit

/**
    Endpoints from Restroom API that will be used in the app
 */
enum RestRoomAPI {
    case getAll
    case search(request: RestroomSearchRequest)
}

// MARK: - EndPointType

extension RestRoomAPI: EndPointType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://www.refugerestrooms.org/api/") else { fatalError("baseURL could not be configured") }
        return url
    }
    
    var path: String {
        switch self {
        case .getAll:
            return "v1/restrooms"
        case .search(_):
            return "v1/restrooms/search"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .getAll:
            return .request
        case .search(request: let request):
            return .requestParameters(nil, urlParameters: ["page": request.page,
                                                           "offset": request.offset,
                                                           "per_page": request.perPage,
                                                           "ada": request.ada,
                                                           "unisex": request.unisex,
                                                           "query": request.query])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }

}
