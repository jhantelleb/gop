//
//  RestroomNetworkManager.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import Foundation

enum NetworkResponse:String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}

struct RestroomNetworkManager {
    var router = Router<RestRoomAPI>()
    
    func getRestrooms(completion: @escaping (_ restrooms: [Restroom], _ error: String?) ->Void) {
        router.request(.getAll) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion([], NetworkResponse.noData.rawValue)
                        return }
                    do {
                        let apiResponse = try JSONDecoder().decode(RestroomResponse.self, from: responseData)
                        completion(apiResponse.restrooms, nil)
                    }catch {
                        completion([], NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func searchRestrooms(request: RestroomSearchRequest, completion: @escaping (_ restrooms: [Restroom], _ error: String?) -> ()) {
        router.request(.search(request: request)) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion([], NetworkResponse.noData.rawValue)
                        return }
                    do {
                        let apiResponse = try JSONDecoder().decode(RestroomResponse.self, from: responseData)
                        completion(apiResponse.restrooms, nil)
                    } catch {
                        completion([], NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let error):
                    print(error)
                    completion([], NetworkResponse.failed.rawValue)
                }
            } else {
                completion([], NetworkResponse.unableToDecode.rawValue)
            }
        }
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}
