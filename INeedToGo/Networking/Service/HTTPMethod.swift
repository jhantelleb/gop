//
//  HTTPMethod.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
