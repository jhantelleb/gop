//
//  HTTPTask.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import Foundation

typealias HTTPHeaders = [String:String]

enum HTTPTask {
    case request
    case requestParameters(_ parameters: Parameters?, urlParameters: Parameters?)
    case requestParametersAndHeaders(bodyParameters: Parameters?, urlParameters: Parameters?, additionalHeaders: HTTPHeaders?)
}
