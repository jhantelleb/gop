//
//  RestroomRequest.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/21/21.
//

import Foundation

/**
    This contains the information to request restrooms
 */
struct RestroomSearchRequest {
    let page: Int
    let perPage: Int
    let offset: Int
    let ada: Bool
    let unisex: Bool
    let query: String
    
    init(page: Int, perPage: Int = 100, offset: Int = 0, ada: Bool, unisex: Bool = true, query: String) {
        self.page = page
        self.perPage = perPage
        self.offset = offset
        self.ada = ada
        self.unisex = unisex
        self.query = query
    }
}
