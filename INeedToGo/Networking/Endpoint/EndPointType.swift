//
//  EndPoint.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import UIKit

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

