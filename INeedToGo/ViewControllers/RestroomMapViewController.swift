//
//  RestroomMapViewController.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/19/21.
//

import MapKit
import CoreLocation
import Contacts

/**
 The Restroom Map View Controller is responsible for displaying the map and the information related to the restrooms
 */
class RestroomMapViewController: UIViewController {
    
    // MARK: - Private Properties
    
    private var service = RestroomNetworkManager()
    
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        mapView.showsUserLocation = true
        return mapView
    }()
    
    private let blurView = UIView()
    private let activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .medium)
        view.color = .white
        view.startAnimating()
        return view
    }()
    
    private let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()
    
    
    private var location: CLLocation
    private var restrooms: [Restroom] = []
    private var page: Int = 1
    private var perPage: Int = 100
    private var offset: Int = 0
    private var availableLocation: AvailableLocation? = AvailableLocation.mockData.first
    private var locationText = "" {
        didSet {
            navigationItem.leftBarButtonItem?.title = locationText
        }
    }
    
    // MARK: - Initialization
    
    init() {
        location = availableLocation?.location ?? CLLocation(latitude: 40.7831, longitude: -73.9712)
        super.init(nibName: nil, bundle: nil)
        title = "Find a Restroom"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - View Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        locationText = AvailableLocation.mockData.first?.locationName ?? ""
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: locationText, style: .plain, target: self, action: #selector(handleChooseLocationTapped))
        let locationButton = UIBarButtonItem(image: #imageLiteral(resourceName: "searchLocation"), style: .plain, target: self, action: #selector(handleLocationTapped))
        let surpriseButton = UIBarButtonItem(image: #imageLiteral(resourceName: "giveaway"), style: .plain, target: self, action: #selector(handleSurpriseTapped))
        navigationItem.rightBarButtonItems = [locationButton, surpriseButton]
        
        view.addSubview(mapView)
        view.addSubview(blurView)
        view.addSubview(activityIndicator)
    
        blurView.backgroundColor = .black.withAlphaComponent(0.80)
        
        mapView.delegate = self
        mapView.register(RestroomAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        setZoomRange()
        reloadMap()
    }
    
    // MARK: - Layout
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mapView.frame = self.view.bounds
        blurView.frame = self.view.bounds
        let frameHeight = view.frame.height
        let frameWidth = view.frame.width
        activityIndicator.frame = CGRect(x: frameWidth / 2.0, y: frameHeight / 2.0, width: activityIndicator.frame.width, height: activityIndicator.frame.height)
    }
    
    // Map Helpers
    
    private func reloadMap(with userLocation: CLLocation? = nil) {
        activityIndicator.startAnimating()
        toggleViews(false)
        
        if userLocation != nil {
            guard let userLocation = userLocation else { return }
            reverseGeocode(userLocation) { locationText in
                self.locationText = locationText
                self.searchRestrooms(locationText: locationText)
            }
        } else {
            self.searchRestrooms(locationText: availableLocation?.queryName ?? "")
        }
    }
    
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.stopUpdatingLocation()
        }
        let userLocation = mapView.userLocation
        self.location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
    }
    
    private func centerToMapLocation() {
        mapView.centerToLocation(location)
    }
    
    private func setZoomRange() {
        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 200000)
        mapView.setCameraZoomRange(zoomRange, animated: true)
    }
    
    private func reverseGeocode(_ location: CLLocation, completion: @escaping (String) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            if let placemark = placemarks?.first {
                completion(placemark.locationText)
            }
        }
    }
    
    // MARK: - Events
    
    @objc private func handleLocationTapped() {
        locationManager.startUpdatingLocation()
        checkLocationServices()
        
        let userLocation = mapView.userLocation.location?.coordinate
        reloadMap(with: CLLocation(latitude: userLocation?.latitude ?? 0.0, longitude: userLocation?.longitude ?? 0.0))
    }
    
    @objc private func handleChooseLocationTapped() {
        // present the available locations VC
        pushAvailableLocations()
    }
    
    @objc private func handleSurpriseTapped() {
        presentSurprise()
    }
    
    // MARK: - API
    
    private func searchRestrooms(locationText: String) {
        service.router.cancel()
        let request = RestroomSearchRequest(page: page, perPage: perPage, offset: offset, ada: true, unisex: true, query: locationText)
        service.searchRestrooms(request: request) { [weak self] restrooms, error in
            guard let self = self else { return }
            
            if error != nil {
                let alert = UIAlertController(title: "Uh Oh", message: error?.description, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { action in
                    self.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.restrooms = restrooms
            DispatchQueue.main.async {
                self.toggleViews(true)
                self.activityIndicator.stopAnimating()
                self.centerToMapLocation()
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                if !restrooms.isEmpty {
                    self.mapView.addAnnotations(restrooms)
                } else {
                    let alert = UIAlertController(title: "Oh no", message: "Sorry, there are no restrooms available in this area \(locationText).", preferredStyle: .alert)
                    let tapAction = UIAlertAction(title: "Tap to make you feel better.", style: .default) { action in
                        self.dismiss(animated: true, completion: nil)
                        self.presentSurprise()
                    }
                    alert.addAction(tapAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - Helpers
    
    private func toggleViews(_ toggle: Bool) {
        DispatchQueue.main.async {
            self.blurView.isHidden = toggle
            self.navigationItem.leftBarButtonItem?.isEnabled = toggle
            
            if let rightBarButtonItems = self.navigationItem.rightBarButtonItems  {
                for button in rightBarButtonItems {
                    button.isEnabled = toggle
                }
            }
        }
    }
    
    private func pushAvailableLocations() {
        let availableLocationsVC = AvailableLocationsViewController(delegate: self)
        navigationController?.pushViewController(availableLocationsVC, animated: true)
    }
    
    private func presentSurprise() {
        let infiniteScrollViewController = InfiniteScrollCollectionViewController()
        self.navigationController?.present(infiniteScrollViewController, animated: true, completion: nil)
    }
    
}

// MARK: - MKMapView

private extension MKMapView {
    
    func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 5000) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: false)
    }
    
}

// MARK: - CLLocationManager Delegate

extension RestroomMapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .notDetermined, .restricted:
            manager.requestWhenInUseAuthorization()
        case .denied:
            break
        case .authorizedAlways, .authorizedWhenInUse:
            manager.requestLocation()
            manager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed \(error)")
    }
}

// MARK: - MKMapViewDelegate
extension RestroomMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let restroom = view.annotation as? Restroom else {
            return
        }
        
        if view.rightCalloutAccessoryView == control {
            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
            restroom.mapItem?.openInMaps(launchOptions: launchOptions)
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        views.forEach {
            if let _ = $0.annotation as? MKUserLocation {
                $0.layer.anchorPointZ = 0
            } else {
                $0.layer.anchorPointZ = 1
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        locationManager.stopUpdatingLocation()
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            self.showToast(message: "You're in \(locationText)!")
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if !view.isKind(of: MKUserLocationView.self) {
            view.backgroundColor = .white
        }
    }
}

// MARK: - AvailableLocationsViewControllerDelegate

extension RestroomMapViewController: AvailableLocationsViewControllerDelegate {
    
    func didChangeLocation(availableLocation: AvailableLocation) {
        self.availableLocation = availableLocation
        navigationItem.leftBarButtonItem?.title = availableLocation.locationName
        self.location = availableLocation.location
        reloadMap()
    }
    
}

// MARK: - CLPlacemark

extension CLPlacemark {
    
    var locationText: String {
        return ("\(self.subLocality ?? "") \(self.administrativeArea ?? "")")
    }
    
}
