//
//  InfiniteScrollCollectionViewController.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/20/21.
//

import UIKit

/**
    This view controller
 */
class InfiniteScrollCollectionViewController: UIViewController {
    
    // MARK: - Private Properties

    private var allFood: [Food] = []
    
    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = UIColor.white
        collection.isScrollEnabled = true
        collection.isPagingEnabled = true
        collection.showsHorizontalScrollIndicator = false
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: collection.frame.size.height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collection.collectionViewLayout = layout
        collection.delegate = self
        collection.dataSource = self
        return collection
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        
        navigationController?.navigationItem.leftBarButtonItem = UIBarButtonItem(systemItem: .close)
        collectionView.register(InfiniteCollectionViewCell.self, forCellWithReuseIdentifier: "main")
        
        createRows()
        collectionView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.frame = self.view.bounds
    }
    
    // MARK: - Private Functions
    
    private func createRows() {
        let food = [Food(name: "Burrito", image: #imageLiteral(resourceName: "burrito")),
                    Food(name: "Croissant", image: #imageLiteral(resourceName: "croissants")),
                    Food(name: "Burger", image: #imageLiteral(resourceName: "cheeseburger")),
                    Food(name: "Chinese Take Out", image: #imageLiteral(resourceName: "chinese_takeout"))]
        
        
        // Construct data for infinite loop
        allFood = food
        guard let firstFood = food.first,
              let lastFood = food.last else { return }
        allFood.append(firstFood)
        allFood.insert(lastFood, at: 0)
    }
    
}

extension InfiniteScrollCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allFood.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "main", for: indexPath) as? InfiniteCollectionViewCell else {
            return UICollectionViewCell()
        }
        let food = allFood[indexPath.row]
        cell.configure(food.name, image: food.image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageFloat = (scrollView.contentOffset.x / scrollView.frame.size.width)
        let pageInt = Int(round(pageFloat))
        
        switch pageInt {
        case 0:
            collectionView.scrollToItem(at: [0, allFood.count - 2], at: .left, animated: false)
        case allFood.count - 1:
            collectionView.scrollToItem(at: [0, 1], at: .left, animated: false)
        default:
            break
        }
    }
    
}
