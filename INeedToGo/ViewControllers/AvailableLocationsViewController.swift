//
//  AvailableLocationsViewController.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/21/21.
//

import CoreLocation
import UIKit

/**
    Lists the locations with available restrooms
 */
class AvailableLocationsViewController: UITableViewController {
    
    weak var delegate: AvailableLocationsViewControllerDelegate?
    
    private let locations = AvailableLocation.mockData
    
    // MARK: - Initialization
    
    init(delegate: AvailableLocationsViewControllerDelegate?) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        title = "Available Locations"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "location")
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UITableViewDelegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "location", for: indexPath)
        cell.textLabel?.text = locations[indexPath.row].locationName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = locations[indexPath.row]
        delegate?.didChangeLocation(availableLocation: location)
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - AvailableLocationsViewControllerDelegate

protocol AvailableLocationsViewControllerDelegate: class {
    
    func didChangeLocation(availableLocation: AvailableLocation)
    
}
