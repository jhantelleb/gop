//
//  InfiniteCollectionViewCell.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/20/21.
//

import UIKit

/**
    Custom Cell for the Infinite Collection View Controller
 */
class InfiniteCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Private Properties
    
    private var label: UILabel = {
       let label = UILabel()
        label.font = UIFont(name: "ProximaNova-Regular", size: 28.0)
        label.textColor = .black
        return label
    }()
        
    private var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.addSubview(imageView)
        self.addSubview(label)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure
    
    func configure(_ message: String = "Food", image: UIImage?) {
        label.text = message
        if let image = image {
            imageView.image = image
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    // MARK: - Layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.frame = self.bounds
        
        label.sizeToFit()
        label.frame = CGRect(x: 20.0, y: 20.0, width: self.contentView.frame.width, height: 40.0)
        
    }
    
}
