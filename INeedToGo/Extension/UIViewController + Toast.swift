//
//  UIViewController + Toast.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/20/21.
//

import UIKit

/**
    Toast extension for all UIViewControllers
 */
extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel()
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.clipsToBounds  =  true
        
        let textSize:CGSize = toastLabel.intrinsicContentSize
        let labelWidth = min(textSize.width, view.frame.width - 40.0)
        toastLabel.frame = CGRect(x: 20.0, y: view.frame.height - 90.0, width: labelWidth + 30.0, height: textSize.height + 20.0)
        toastLabel.center.x = view.center.x
        toastLabel.layer.cornerRadius = toastLabel.frame.height / 2.0
        view.addSubview(toastLabel)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            UIView.animate(withDuration: 1.0, animations: {
                toastLabel.alpha = 0
            }) { (_) in
                toastLabel.removeFromSuperview()
            }
        }
    }
}




