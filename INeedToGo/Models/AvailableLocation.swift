//
//  AvailableLocation.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/21/21.
//

import Foundation
import CoreLocation

/**
 Mock Data for Available Locations
 */
struct AvailableLocation {
    let locationName: String
    let location: CLLocation
    let queryName: String
    
    static var mockData: [AvailableLocation] {
        return [AvailableLocation(locationName: "New York", location: CLLocation(latitude: 40.7580, longitude: -73.9855), queryName: "New York NY USA"), AvailableLocation(locationName: "San Fran, CA", location: CLLocation(latitude: 37.7842421, longitude: -122.4103459), queryName: "San Francisco CA USA"), AvailableLocation(locationName: "Portland, OR", location: CLLocation(latitude: 45.5226351, longitude: -122.6779993), queryName: "Portland OR USA"), AvailableLocation(locationName: "New Jersey", location: CLLocation(latitude: 40.2225359, longitude: -74.6425082), queryName: "New Jersey USA")]

    }
}
