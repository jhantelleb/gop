//
//  Restroom.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/18/21.
//

import UIKit
import MapKit
import Contacts

class Restroom: NSObject, Decodable, MKAnnotation {
    
    let name: String
    let street: String
    let city: String
    let state: String
    let accessible: Bool
    let unisex: Bool
    let changingTable: Bool
    let latitude: CGFloat
    let longitude: CGFloat
    var coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    
    var cityState: String {
        return "\(city), \(state)"
    }
    
    var mapItem: MKMapItem? {
      let addressDict = [CNPostalAddressStreetKey: name]
      let placemark = MKPlacemark(
        coordinate: coordinate,
        addressDictionary: addressDict)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = name
      return mapItem
    }
    
    enum RestroomCodingKeys: String, CodingKey {
        case name
        case address
        case street
        case city
        case state
        case accessible
        case unisex
        case changingTable = "changing_table"
        case latitude
        case longitude
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RestroomCodingKeys.self)
        
        name = try container.decode(String.self, forKey: .name)
        street = try container.decode(String.self, forKey: .street)
        city = try container.decode(String.self, forKey: .city)
        state = try container.decode(String.self, forKey: .state)
        accessible = try container.decode(Bool.self, forKey: .accessible)
        unisex = try container.decode(Bool.self, forKey: .unisex)
        changingTable = try container.decode(Bool.self, forKey: .changingTable)
        latitude = try container.decode(CGFloat.self, forKey: .latitude)
        longitude = try container.decode(CGFloat.self, forKey: .longitude)
        coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        title = name
        subtitle = "\(street) \(city) \(state)"
    }
  
}

// MARK: - Annotation Call Out

extension Restroom {
    
    enum RestroomFeature {
        case accessible
        case changingTable
        case unisex
        
        var icon: UIImage {
            switch self {
            case .accessible:
                return #imageLiteral(resourceName: "wheelchair")
            case .changingTable:
                return #imageLiteral(resourceName: "changing_table")
            case .unisex:
                return #imageLiteral(resourceName: "unisex")
            }
        }
    }
    
    var featureImages: [UIImage] {
        var images: [UIImage] = []
        if self.accessible {
            images.append(RestroomFeature.accessible.icon)
        }
        
        if self.changingTable {
            images.append(RestroomFeature.changingTable.icon)
        }
        
        if self.unisex {
            images.append(RestroomFeature.unisex.icon)
        }
        
        return images
    }
    
}

