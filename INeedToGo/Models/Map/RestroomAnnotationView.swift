//
//  RestroomMapAnnotation.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/19/21.
//

import MapKit
import Contacts

/**
    Custom Restroom Annotation View and Callout
 */
class RestroomAnnotationView: MKAnnotationView {
    override var annotation: MKAnnotation? {
      willSet {
        guard let restroom = newValue as? Restroom else {
          return
        }
        let mapButtonSize: CGFloat = 48.0
        
        canShowCallout = true
        calloutOffset = CGPoint(x: -5, y: 5)
        
        // Right Call Out Accessory - Directions
        let mapButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: mapButtonSize, height: mapButtonSize)))
        mapButton.setBackgroundImage(#imageLiteral(resourceName: "Map"), for: .normal)
        rightCalloutAccessoryView = mapButton
        image =  #imageLiteral(resourceName: "toilet")
        layer.cornerRadius = mapButtonSize / 2
        backgroundColor = .white
        
     
        let view = UIView()
        let views = ["view": view]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view(30)]", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(20)]", options: [], metrics: nil, views: views))
        
        var imageViews: [UIImageView] = []
        var xPos: CGFloat = 0.0
     
        for image in restroom.featureImages {
            let imageView = UIImageView(image: image)
            imageView.frame = CGRect(x: xPos, y: 0.0, width: 12.0, height: 15.0)
            imageView.contentMode = .scaleAspectFill
            xPos += (imageView.layer.frame.height + 4.0)
            imageViews.append(imageView)
        }
        
        for image in imageViews {
            view.addSubview(image)
        }
        
        detailCalloutAccessoryView = view
      }
    }
}

