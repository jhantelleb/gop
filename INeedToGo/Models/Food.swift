//
//  Food.swift
//  INeedToGo
//
//  Created by Jhantelle Belleza on 5/20/21.
//

import UIKit

struct Food {
    let name: String
    let image: UIImage?
}
