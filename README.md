# README #

This app is a simple project that shows you the available restrooms in your area. 

### Views ###

* Find Restroom - Map
- This map shows the restrooms available for the specified location. Tapping a location shows the name and accesibility feature of the restroom. It also gives you a direction once you tap the map icon in the call out.

* Available Locations 
- This is mock data, when I was testing the API, the query string needs to be consistent. "New York" is not "New York City" location. It also shows other locations with the name "New York" on it. So I've decided for this purpose, I'll mock the cities where I want to search.

* Infinite Scroll View
- The data source for this one can be switched to an API Endpoint with images. Unfortunately, I ran out of time and wasn't able to hook up an API for it. There's 2 entry points for this view. The navigation surprise button and when there's no restrooms near the user's location.

### Buttons ###

* City, State
	- When tapped, this shows the available locations (mocked).

* User Location
	- The user location is toggled by the location navigation button on the top right. It takes you to the user location and presents the restroom near you if available. If the restrooms are not available, a surprise awaits you.

* Surprise button
	- This triggers the infinite scroll view. Tap it! 

#### API ####

Restroom Search - https://www.refugerestrooms.org/api/docs/

* Search Endpoint 
	- There was an available endpoint that takes the coordinates. But the radius and span cannot be changed so I decided to use the Search endpoint instead. The Search endpoint has pagination, but I had an issue when fetching more than 100 rows so I capped it to 100.
	
### Layout ###
I did not use third party frameworks to layout my views. My views are not that elaborat so I mostly used the standard views (i.e. annotation views) and layout using frames.

### Networking ###

I also did not a third party framework to fetch the API. Since I was only using one endpoint for fetching, I decided to write what I'm used to but elevated it a little bit to eliminate boiler plate code in case another API will be added. 

### How do I get set up? ###

* Summary of set up
	You'll need to install Xcode to run the project

### Who do I talk to? ###

* Repo owner or admin
	- For questions and concerns, please contact Jhantelle Belleza (jhantelle.belleza@gmail.com)
